#include <Arduino.h>
#ifndef SENSOR_H
#define SENSOR_H
class Sensor{
  private:
  int _pin;
  public:
  Sensor(int pin);

  int GetAnalogRead(); 

  bool GetDigitalRead();
};

#endif

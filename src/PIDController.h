
#ifndef PIDCONTROLLER_H
#define PIDCONTROLLER_H

class PIDController
{
protected:
  float _Kp;
  float _Ki;
  float _Kd;
  float _maxVal;
  float _minVal;
  float _currError;
  float _prevError;
  float _deltaT_ms;
  float _output;
public:

  PIDController (float Kp, float Ki, float Kd, float maxVal, float minVal, float deltaT_ms);

  void CalcOutput (float desiredVal, float currentVal); // This function

  void SetKp (float kp);

  void SetKi (float ki);

  void SetKd (float kd);

  void ResetController();

  float GetKp ();

  float GetKi ();

  float GetKd ();

  float GetControlAction ();
};
#endif

#ifndef MOTORCONTROLLER_H
#define MOTORCONTROLLER_H

#include "Motor.h"
#include "PIDController.h"
#include "Encoder.h"
#include "Repeater.h"

class MotorController{
    private:
        Motor* _mot;
        PIDController* _pidController;
        Encoder* _enc;
        Repeater _repeater;
        int _pulsePerSecond;
        int _previousPulses;
        bool _initialBrakingMode;



    public:
        MotorController(Motor* mot, PIDController* pidController, Encoder* enc);
        
        // Controller Methods

        void SetEnable(bool enable);

        void SetSpeed(int pulsePerMS, float delta_ms);

        // Motor Methods

        void SetDirection(bool direction);

        void SetBrakingMode(bool applyBraking);

        void SetSleepMode(bool isSleepMode);

        void TEMPSetPWM(int pwm);

        bool GetDirection();

        bool GetBrakingMode();

        bool GetSleepMode();

        bool GetEnable();

        float GetPulsesPerSec(float delta_ms);

        bool ApplyBrakes();



};
#endif
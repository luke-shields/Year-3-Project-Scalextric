#ifndef IMU_H
#define IMU_H

#include <Arduino.h>
#include <Wire.h>

class IMU{

    public:
    IMU(TwoWire& wire);
    ~IMU();

    int begin();
    void end();

    // Controls whether a FIFO is continuously filled, or a single reading is stored.
    // Defaults to one-shot.   
    void setContinuousMode();
    void setOneShotMode();

    // Accelerometer
    int readAcceleration(float& x, float& y, float& z); // Results are in G (earth gravity).
    int accelerationAvailable(); // Number of samples in the FIFO.

    // Gyroscope
    int readGyroscope(float& x, float& y, float& z); // Results are in degrees/second.
    int gyroscopeAvailable(); // Number of samples in the FIFO.

    // Magnetometer
    int readMagneticField(float& x, float& y, float& z); // Results are in uT (micro Tesla).
    int magneticFieldAvailable(); // Number of samples in the FIFO.

    private:
    TwoWire* _wire;
    int readRegister(uint8_t slaveAddress, uint8_t address);
    int readRegisters(uint8_t slaveAddress, uint8_t address, uint8_t* data, size_t length);
    int writeRegister(uint8_t slaveAddress, uint8_t address, uint8_t value);
    bool continuousMode;
};
#endif
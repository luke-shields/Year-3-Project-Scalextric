#include "Motor.h"

Motor::Motor(int enablePin, int phasePin, int mode1Pin, int mode2Pin, int nSleepPin, int nFaultPin){
    _phasePin = phasePin;
    _mode1Pin = mode1Pin;
    _mode2Pin = mode2Pin;
    _nSleepPin = nSleepPin;
    _nFaultPin = nFaultPin;
    _enablePin = enablePin;
    
    // Setup Pin Modes
    pinMode(_phasePin, OUTPUT);
    pinMode(_mode1Pin, OUTPUT);
    pinMode(_mode2Pin, OUTPUT);
    pinMode(_nSleepPin, OUTPUT);

    pinMode(_nFaultPin, INPUT);
   
    // Configure original values
    SetPWM(0);
    SetDirection(LOW);
    SetBrakingMode(HIGH);
    SetSleepMode(LOW);
    
  }

    // Setters
  void Motor::SetEnable(bool isEnabled){
    _isEnabled = isEnabled;
    if(_isEnabled){
      analogWrite(_enablePin, _pwm);
    } else {
      analogWrite(_enablePin, 0);
    }
  }

  void Motor::SetPWM(int pwm){
    if(pwm<0){
      pwm = 0;
    } else if (pwm>255){
      pwm = 255;
    }
    _pwm = pwm;
    if(_isEnabled){
      analogWrite(_enablePin, _pwm);
    } 
  }

  void Motor::SetDirection(bool direction){
    _direction = direction;
    digitalWrite(_phasePin, _direction);
  }

  void Motor::SetBrakingMode(bool applyBraking){
    _brakingMode = applyBraking;
    if(_brakingMode == true){
      digitalWrite(_mode1Pin, HIGH);
      digitalWrite(_mode2Pin, LOW); // Apply low side drivers
    } else {
      digitalWrite(_mode1Pin, LOW);
      digitalWrite(_mode2Pin, LOW); // Not used as braking is off
    }
  }

  void Motor::SetSleepMode(bool isSleepMode){
    _sleepMode = isSleepMode;
    digitalWrite(_nSleepPin, !_sleepMode); // Inverted
  }


  

  // Getters

  bool Motor::GetEnable(){
    return _isEnabled;
  }

  bool Motor::GetDirection(){
    return _direction;
  }

  bool Motor::GetBrakingMode(){
    return _brakingMode;
  }

  bool Motor::GetSleepMode(){
    return _sleepMode;
  }

  byte Motor::GetPWM(){
    return _pwm;
  }

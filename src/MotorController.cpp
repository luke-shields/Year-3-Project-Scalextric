#include "MotorController.h"
#include <Arduino.h>

MotorController::MotorController(Motor* mot, PIDController* pidController, Encoder* enc): _mot(mot), _pidController(pidController), _enc(enc){
    _initialBrakingMode = _mot->GetBrakingMode();
}

void MotorController::SetEnable(bool enable){
    _mot->SetEnable(enable);
}

void MotorController::SetSpeed(int pulsesPerSec, float delta_ms){
    if(_mot->GetEnable() == false){
        // If motor not enabled PID should not be performed
        _previousPulses = _enc->GetPulses();
    } else {
        int currPulses = _enc->GetPulses();
        float currPulsesPerSec = 1000 * ((currPulses - _previousPulses) / (delta_ms)); // Calculates current speed
        _previousPulses = currPulses;

        _pidController->CalcOutput(pulsesPerSec, currPulsesPerSec); // Calculates PID control action
        _mot->SetPWM(_pidController->GetControlAction()); // Implements control action
    }

}

void MotorController::SetDirection(bool direction){
    _mot->SetDirection(direction);
}

void MotorController::SetBrakingMode(bool applyBraking){
    _mot->SetBrakingMode(applyBraking);

}

void MotorController::SetSleepMode(bool isSleepMode){
    _mot->SetSleepMode(isSleepMode);
}

void MotorController::TEMPSetPWM(int pwm){
    _mot->SetPWM(pwm);
}

bool MotorController::GetDirection(){
    return _mot->GetDirection();
}

bool MotorController::GetBrakingMode(){
    return _mot->GetBrakingMode();
}

bool MotorController::GetSleepMode(){
    return _mot->GetSleepMode();
}

bool MotorController::GetEnable(){
    return _mot->GetEnable();
}

float MotorController::GetPulsesPerSec(float delta_ms){
    // Only to be used while braking
    // Caution - Using this function disrupts SetSpeed()
    int currPulses = _enc->GetPulses();
    float toReturn = 1000 * (currPulses-_previousPulses)/(delta_ms);
    _previousPulses = currPulses;
    return toReturn;
}

bool MotorController::ApplyBrakes(){
    // This function applies brakes to the motor
    // It returns false until the motor has stopped then true is returned
    SetEnable(false);
    SetBrakingMode(true);
    _mot->SetPWM(0);

    if(GetPulsesPerSec(10) > 0){
        return(false);
    } else {
        if(_initialBrakingMode == false){
            // Returns braking mode to initial value
            SetBrakingMode(false);
        }
        SetEnable(true);
        return(true);
    }
}